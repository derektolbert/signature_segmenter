import os
import cv2
import random
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.models import load_model

from src.generator import DataGenerator, loadImage

# CONSTANTS
DATA_DIR = "/home/aptus/Clients/Pinetree/Stax/signature_detection/segmented/test/"
MODEL_FILE = "./models/segmenter.hdf5"
IMG_SIZE = (256, 256, 1)

# Load model
model = load_model(MODEL_FILE)

# Get test image files and randomly shuffle
test_dir = DATA_DIR + "raw/"
test_files = [ test_dir + f for f in os.listdir(test_dir) ]

label_dir = DATA_DIR + "segmented/"
test_labels = [ label_dir + f for f in os.listdir(label_dir) ]

# Load image, predict, and display
gen = DataGenerator(IMG_SIZE, .5, 32, 32, "train")
X, y = gen.__getitem__(0)
for i, x in enumerate(X):

    # Get pred, apply Otsu threshold
    pred = model.predict(np.array([x]))[0]
    
    orig_size = (256, 128)
    pred = cv2.resize(pred, orig_size)

    # Resize original
    x = cv2.resize(x, orig_size)

    plt.subplot(121)
    plt.title("Original")
    plt.imshow(x, cmap="gray")

    plt.subplot(122)
    plt.title("Predicted")
    plt.imshow(pred, cmap="gray")

    plt.show()
    plt.close()
