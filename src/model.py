# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md in Project root for project license
"""Module containing model definition for unsupervised depth learning."""

from dataclasses import dataclass
from typing import Tuple

from tensorflow.keras.models import Model, load_model
import tensorflow.keras.layers as kl
from tensorflow.keras.optimizers import Nadam
from tensorflow.keras.regularizers import l2


def createModel() -> Model:
    """
    Create Densely connected FCN similar to the hundred layers tiramisu.

    each parameter has a default that is a sane value

    Parameters
    ----------
        num_dense_blocks -> number of downsampling and upsampling steps
        convs_per_block -> number of convolution layers in a dense block
        filts_per_conv -> number of filters per convolutional layer to start out with
        filter_step -> number of filters to increase by each time there is a downsampling step
    """
    # Create input placeholder tensors
    in_layer = kl.Input((None, None, 1))
    imsTensor = in_layer
    print('\n\nims tensor shape is ', imsTensor.shape)

    # Initialize the skip connections and set a schedule for each of the dense parameters.
    skips = []
    convSched = [4, 5, 5, 6, 6]
    filtSched = [5, 6, 7, 7, 7]
    # dila_sched = list(range(5, 0, -1))
    dilaSched = [1 for i in range(len(convSched))]

    # Create the downsampling network.
    for i in range(len(convSched)):
        denseParams = DenseParams(convSched[i], filtSched[i], dilaSched[i])
        imsTensor = denseBlock(imsTensor, denseParams)
        imsTensor = kl.MaxPooling2D()(imsTensor)
        skips.append(imsTensor)

    # Reverse everything so we use the same schedule upsampling, and have the right size
    # skip connections.
    skips = list(reversed(skips))
    convSched = list(reversed(convSched))
    filtSched = list(reversed(filtSched))
    dilaSched = list(reversed(dilaSched))

    # Create the bottleneck tensor, the most compressed and abstract representation of the data.
    imsTensor = denseBlock(imsTensor, denseParams)

    # Create the upsampling network.
    for inputs in zip(skips, convSched, filtSched, dilaSched):
        skip, *params = inputs
        denseParams = DenseParams(*params)
        inputs = kl.concatenate([skip, imsTensor])
        imsTensor = kl.UpSampling2D()(inputs)
        imsTensor = denseBlock(imsTensor, denseParams)

    signature_layer = kl.Conv2D(filters=1, kernel_size=3, padding='same',
                          activation='sigmoid')(imsTensor)

    # Create and compile the model.
    signature_model = Model(in_layer, signature_layer)
    signature_model.compile(optimizer=Nadam(), loss='binary_crossentropy',
                        metrics=['mean_squared_error', 'mean_absolute_error', 'binary_accuracy'])

    return signature_model


def denseBlock(input_tensor, dense_params):
    """Create a dense block acting on the inputs."""
    outputs = []
    inputs = [input_tensor]

    for _ in range(dense_params.num_convs):
        conved = kl.Conv2D(filters=dense_params.num_filts, kernel_size=3, activation='elu',
                           padding='same', dilation_rate=dense_params.dilation_rate,
                           kernel_regularizer=l2(2e-3))(input_tensor)
        conved = kl.GaussianNoise(0.03)(conved)
        outputs.append(conved)
        inputs.append(conved)
        input_tensor = kl.concatenate(inputs[:])
        input_tensor = kl.BatchNormalization()(input_tensor)

    output_tensor = kl.concatenate(outputs[:])
    output_tensor = kl.BatchNormalization()(output_tensor)
    return output_tensor


@dataclass
class DenseParams:
    """Class for keeping track of dense block parameters."""
    num_convs: int
    num_filts: int
    dilation_rate: int