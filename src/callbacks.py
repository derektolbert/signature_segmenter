import numpy as np
from tensorflow.keras.callbacks import Callback

# CONSTANTS
SAVE_DIR = "/home/aptus/Clients/Pinetree/Stax/signature_detection/signature_segmenter/models/"

class SaveBestModel(Callback):

    # Track best validation loss
    best_val_loss = np.inf

    def on_epoch_end(self, epoch, logs):

        # Save model by validation loss
        if np.less(logs.get("val_loss", np.inf), self.best_val_loss):

            print(f"\n\n SAVING MODEL \n\n")
            self.best_val_loss = logs.get("val_loss")

            # Save model
            self.model.save(SAVE_DIR + "segmenter.hdf5")

        return super().on_epoch_end(epoch, logs=logs)