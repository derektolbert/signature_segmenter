
import os
import cv2
import random
import numpy as np
from typing import Tuple
from tensorflow.keras.utils import Sequence
from sklearn.model_selection import train_test_split
from tensorflow.keras.preprocessing.image import ImageDataGenerator


augment_args = dict(
    fill_mode="nearest",
    horizontal_flip=True,
    vertical_flip=True,
    rotation_range=10,
    shear_range=0.01,
    zoom_range=0.01,
)

idg = ImageDataGenerator(**augment_args)

def getRandomSample(docs: list, sigs: list, augment: bool = False):
    
    doc = cv2.imread(random.sample(docs, 1)[0])
    sig = cv2.imread(random.sample(sigs, 1)[0])

    if doc is None or sig is None:
        return getRandomSample(docs, sigs, augment)

    if augment:
        doc_transform = idg.get_random_transform(doc.shape)
        doc = idg.apply_transform(doc, doc_transform)

        sig_transform = idg.get_random_transform(sig.shape)
        sig = idg.apply_transform(sig, sig_transform)

    doc = cv2.cvtColor(doc, cv2.COLOR_BGR2GRAY)
    sig = cv2.cvtColor(sig, cv2.COLOR_BGR2GRAY)

    return doc, sig

def loadImage(filename: str, img_size: Tuple[int, int]) -> Tuple[np.ndarray, Tuple[float, float]]:

    # Randomly select doc and sig
    img = cv2.imread(filename, 0) / 255.
    
    orig_size = img.shape

    # Resize and add dims
    new_size = img_size[1], img_size[0]
    generated_doc = np.expand_dims(cv2.resize(img, new_size), -1)

    return generated_doc, orig_size

class DataGenerator(Sequence):

    def __init__(self, img_size: Tuple[int, int, int], null_percent: float, batch_size: int, epoch_length: int, mode: str = "train"):
        
        self.img_size = img_size
        self.batch_size = batch_size
        self.epoch_length = epoch_length
        self.null_percent = null_percent
        self.train_mode = mode == "train"

        # Read docs and signatures from disk
        self.docs = ["./data/docs/" + f for f in os.listdir("./data/docs/")]
        sigs = ["./data/signatures/" + f for f in os.listdir("./data/signatures/")]

        # Split signatures into train/val sets
        sig_train, sig_val = train_test_split(sigs, test_size=0.2, random_state=8)

        if self.train_mode:
            self.sigs = sig_train
        else:
            self.sigs = sig_val

        self.on_epoch_end()

    def __len__(self):
        return self.epoch_length

    def __getitem__(self, batch_index):

        # Get data
        X = np.zeros((self.batch_size, *self.img_size))
        y = np.zeros((self.batch_size, *self.img_size))

        for i in range(self.batch_size):

            # Update list of data
            X[i], y[i] = self.loadImage()

        return X, y

    def getNull(self):

        # Randomly select a document to sample
        doc, sig = getRandomSample(self.docs, self.sigs, False)

        # Skip if sig is taller or wider than doc
        doc_h, doc_w = doc.shape
        sig_h, sig_w = sig.shape

        if sig_h > doc_h or sig_w > doc_w:
            return self.getNull()

        # Find random place on doc to use as background
        rand_x = random.randrange(0, doc_w - sig_w)
        rand_y = random.randrange(0, doc_h - sig_h)
        generated_doc = doc[rand_y: rand_y + sig_h, rand_x: rand_x + sig_w]

        # Create an array of zeros (no signature in image)
        bin_sig = np.zeros(generated_doc.shape)

        # Resize and add dims
        new_size = self.img_size[1], self.img_size[0]
        generated_doc = np.expand_dims(cv2.resize(generated_doc, new_size), -1)
        bin_sig = np.expand_dims(cv2.resize(bin_sig.astype("float32"), new_size), -1)

        return generated_doc, bin_sig


    def loadImage(self, ignore_null: bool = False) -> Tuple[np.ndarray, np.ndarray]:

        # Randomly generate null images (no signature overlays) if asked
        if not ignore_null and random.random() < self.null_percent:
            return self.getNull()

        # Randomly select doc and sig, augment if in train mode
        doc, sig = getRandomSample(self.docs, self.sigs, self.train_mode)

        # Skip docs that are too dark
        if np.sum(doc) < 100 or doc is None or sig is None:

            # Dont re-check for null 
            return self.loadImage(True)

        # Skip if sig is taller or wider than doc
        doc_h, doc_w = doc.shape
        sig_h, sig_w = sig.shape

        if sig_h > doc_h or sig_w > doc_w:

            # Dont re-check for null
            return self.loadImage(True)

        # Create mask with signature
        bin_sig = cv2.GaussianBlur(sig, (5,5), 0)
        _, bin_sig = cv2.threshold(bin_sig, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        bin_sig = bin_sig < 0.5

        # Find random place on doc to use as background
        rand_x = random.randrange(0, doc_w - sig_w)
        rand_y = random.randrange(0, doc_h - sig_h)
        generated_doc = doc[rand_y: rand_y + sig_h, rand_x: rand_x + sig_w]

        # Place signature on doc background
        masked_coords = np.where(bin_sig)
        generated_doc[masked_coords] = sig[masked_coords]
        generated_doc =  generated_doc / 255.0
        
        # Resize and add dims
        new_size = self.img_size[1], self.img_size[0]
        generated_doc = np.expand_dims(cv2.resize(generated_doc, new_size), -1)
        bin_sig = np.expand_dims(cv2.resize(bin_sig.astype("float32"), new_size), -1)

        return generated_doc, bin_sig