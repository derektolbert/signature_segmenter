
import os
import cv2
import random
import numpy as np

# CONSTANTS
DATA_DIR = "../data/"
DOC_DIR = DATA_DIR + "docs/"
SIGNATURE_DIR = DATA_DIR + "signatures/"

# Get all filenames for docs and signatures
doc_files = [ DOC_DIR + f for f in os.listdir(DOC_DIR) ]
sig_files = [ SIGNATURE_DIR + f for f in os.listdir(SIGNATURE_DIR) ]

import matplotlib.pyplot as plt
for i in range(len(doc_files)):

    # Load images
    doc = cv2.imread(doc_files[i], 0)
    sig = cv2.imread(sig_files[i], 0)

    # Skip docs that are too dark
    if np.sum(doc) < 100:
        continue

    # Skip if sig is taller or wider than doc
    doc_h, doc_w = doc.shape
    sig_h, sig_w = sig.shape

    if sig_h > doc_h or sig_w > doc_w:
        continue

    # Create mask with signature
    bin_sig = cv2.GaussianBlur(sig, (5,5), 0)
    _, bin_sig = cv2.threshold(bin_sig, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    bin_sig = bin_sig < 0.5

    # Find random place on doc to use as background
    rand_x = random.randrange(0, doc_w - sig_w)
    rand_y = random.randrange(0, doc_h - sig_h)
    doc_background = doc[rand_y: rand_y + sig_h, rand_x: rand_x + sig_w]

    # Place signature on doc background
    masked_coords = np.where(bin_sig)
    doc_background[masked_coords] = sig[masked_coords]

    plt.subplot(121)
    plt.imshow(sig, cmap="gray")

    plt.subplot(122)
    plt.imshow(doc_background, cmap="gray")

    plt.show()
