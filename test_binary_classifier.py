import numpy as np
import tensorflow as tf

from src.binary_generator import DataGenerator

# CONSTANTS
MODEL_FILE = "./models/classifier.hdf5"
IMG_SIZE = (256, 256, 3)

# Load image, predict, and display
val_gen = DataGenerator(IMG_SIZE, .5, 32, "val")
model = tf.keras.models.load_model(MODEL_FILE)

import matplotlib.pyplot as plt
X, y = val_gen.__getitem__(0)
for i, x in enumerate(X):

    pred = np.round(model.predict(np.array([x]))[0], 0)

    plt.imshow(x)
    plt.title(f"Is there a signature? {pred}")
    plt.show()

    


