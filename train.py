from src.model import createModel
from src.generator import DataGenerator
from src.callbacks import SaveBestModel

# CONSTANTS
DATA_DIR = "/home/aptus/Clients/Pinetree/Stax/signature_detection/segmented/"
IMG_SIZE = (128, 256, 1)
BATCH_SIZE = 16
SEED = 8
EPOCH_LENGTH = 1000
NUM_EPOCHS = 100
NULL_PERCENT = 0.5

# GENERATORS
train_gen = DataGenerator(IMG_SIZE, NULL_PERCENT, BATCH_SIZE, EPOCH_LENGTH)

import matplotlib.pyplot as plt
X, y = train_gen.__getitem__(0)

for i, _ in enumerate(X):

    plt.subplot(121)
    plt.imshow(X[i])

    plt.subplot(122)
    plt.imshow(y[i])

    plt.show()

# Create model and start training
model = createModel()

model.summary()

model.fit_generator(
    generator = train_gen,
    steps_per_epoch = (EPOCH_LENGTH // BATCH_SIZE),
    validation_data = train_gen,
    validation_steps = 100,
    epochs = 100,
    callbacks = [SaveBestModel()]
)