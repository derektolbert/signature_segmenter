import tensorflow as tf
from tensorflow.keras.applications.resnet50 import ResNet50

from src.binary_generator import DataGenerator

# CONSTANTS
TRAIN_DIR = "../segmented/train/raw/"
VAL_DIR = "../segmented/validation/raw/"
MODEL_FILE = "./models/segmenter.hdf5"
IMG_SIZE = (256, 256, 3)

# Load image, predict, and display
train_gen = DataGenerator(IMG_SIZE, .5, 32, TRAIN_DIR, "train")
val_gen = DataGenerator(IMG_SIZE, .5, 32, VAL_DIR, "val")

# Load model
base_model = ResNet50(weights="imagenet", input_shape=IMG_SIZE, include_top=False, pooling="max")
base_model.trainable = False
classifier = tf.keras.layers.Dense(256, activation="relu")(base_model.output)
classifier = tf.keras.layers.Dropout(0.5)(classifier)
classifier = tf.keras.layers.Dense(128, activation="relu")(classifier)
classifier = tf.keras.layers.Dropout(0.5)(classifier)
classifier = tf.keras.layers.Dense(32, activation="relu")(classifier)
classifier = tf.keras.layers.Dropout(0.5)(classifier)
classifier = tf.keras.layers.Dense(1, activation="sigmoid")(classifier)
model = tf.keras.models.Model(inputs=base_model.input, outputs=classifier)

model.summary()
model.compile(
    loss="binary_crossentropy", 
    optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3),
    metrics=["accuracy"]
)

# Checkpoints
checkpoint = tf.keras.callbacks.ModelCheckpoint(
    './models/classifier.hdf5', 
    monitor='val_accuracy', 
    save_best_only=True, 
    mode='max'
)

model.fit_generator(
    train_gen,
    steps_per_epoch=100,
    validation_data=val_gen,
    validation_steps=32,
    epochs=30,
    callbacks=[checkpoint]
)